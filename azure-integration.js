const ComputerVisionClient = require('@azure/cognitiveservices-computervision').ComputerVisionClient;
const ApiKeyCredentials = require('@azure/ms-rest-js').ApiKeyCredentials;
const sleep = require('util').promisify(setTimeout);

const key = process.argv[3];
const endpoint = process.argv[2];

const visualFeatures = [
    "Faces",
];

const computerVisionClient = new ComputerVisionClient(
    new ApiKeyCredentials({ inHeader: { 'Ocp-Apim-Subscription-Key': key } }), endpoint);

const computerVision = async (url, name, total, i) => {
    await sleep(1)
    process.stdout.write("\r\x1b[K")
    process.stdout.write(`Photo: ${name} is analyzing... ${i}/${total}`)
    try {
        return await computerVisionClient.analyzeImage(url, { visualFeatures });
    } catch (e) {
       await sleep(5000)
       return computerVision(url, name);
    }
}

module.exports = async (url, name, total, i) => {
    return await computerVision(url, name, total, i)
}
