const firebaseConfig = require('./firebase-conf');
const exec = require('child_process').exec;
const pathHelper = require('path');
const { getStorage, ref, uploadBytes, getDownloadURL  } = require("firebase/storage");
const fs = require("fs");
const firebase = require('firebase/app');

firebase.initializeApp(firebaseConfig)

function getAllFilesRecursive(path) {
  return new Promise(resolve => exec(`ls ${path}`, async (err,stdout,stderr) => {
      const items = stdout.split('\n').filter(item => !!item);
      if (stdout.trim() !== path.trim()) {
          const files = []
          for (let i = 0; i < items.length; i++) {
              let item = items[i];
              item = pathHelper.join(path, item)
              const results = await getAllFilesRecursive(item)
              files.push(results)
          }
          resolve(files.flat(Infinity))
      } else {
          resolve(items);
      }
  }))
}

async function uploadFiles(files) {
    const storage = getStorage();
    for (const file of files) {
        console.log('File ' + file + ' is being uploaded...');
        const storageRef = ref(storage, file);
        const blob = await readFileToBlob(file);
        await uploadBytes(storageRef, blob);
    }
}

async function readFileToBlob(pathToFile) {
    return new Promise(resolve => {
        fs.readFile(pathToFile, (error, data) => {
            if(error) {
                throw error;
            }
            const buffer = Buffer.from(data);
            resolve(Uint8Array.from(buffer).buffer);
        });
    });
}

const getFileUrlFromStorage = async (path) => {
    const storage = getStorage();
    const storageRef = ref(storage, path);
    return await getDownloadURL(storageRef);
}

const uploadAssets = async (pathToAssets) => {
    const files = (await getAllFilesRecursive(pathToAssets)).filter(path => path.includes('blurred') || path.includes('resized'));
    console.log('Files have been found.');
    await uploadFiles(files)
    console.log('Files have been uploaded totally.');
    return files;
}

module.exports = {
    getFileUrlFromStorage,
    uploadAssets
};