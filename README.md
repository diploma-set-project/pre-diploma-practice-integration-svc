# pre-diploma-practice-integration-svc

Integration Service

Azure integration to get result of analyzing of computer vision 

Before running necessary to add `firebase-conf.json` with firebase config to use firebase storage.

To run project:

```bash

node . AZURE_API_HOST AZURE_API_KEY PATH_TO_ASSETS_FOLDER MONGO_BASE_URL

```

Service will recognize date and push it into mongo database.