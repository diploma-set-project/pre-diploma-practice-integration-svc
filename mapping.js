const mongoose = require('mongoose');
const {Schema} = require("mongoose");

const ImageAnalyze = mongoose.model('ImageAnalyzePreDiplomaFaces', new Schema({
    foundFace: Boolean,
    facesAmount: Number,
    age: Number,
    gender: String,
    name: String,
    level: String,
}, {
    timestamps: true
}));

module.exports = async function(resultInfo, name) {
    const imageAnalyze = new ImageAnalyze(toMap(resultInfo, name))
    await imageAnalyze.save()
}

function toMap(resultInfo, name) {
    return {
        foundFace: !!resultInfo.faces.length,
        facesAmount: resultInfo.faces.length,
        age: resultInfo.faces.length ? resultInfo.faces[0].age : null,
        gender: resultInfo.faces.length ? resultInfo.faces[0].gender : null,
        name,
        level: getLevel(name),
    }
}

function getLevel(path) {
    return path.match(/faces-(.*?)\//g)[0].replace('/', '')
}


