const { uploadAssets, getFileUrlFromStorage } = require('./firebase')
const mongoose = require("mongoose");
const computerVision = require("./azure-integration");
const saveResult = require("./mapping");

async function main() {
    await mongoose.connect(process.argv[5]).then(_ => console.log('Mongo Connected!!!'));
    const filePaths = await uploadAssets(process.argv[4]);
    console.log('Preparing links...');
    const publicFileUrls = await Promise.all(filePaths.map((path) => getFileUrlFromStorage(path)));
        await publicFileUrls.reduce((acc, url, i, items) =>
                acc
                    .then(() => computerVision(url, filePaths[i], items.length, i))
                    .then(result => saveResult(result, filePaths[i])),
            Promise.resolve());

    console.log('Analyzed results!');
    process.exit(0);
}

main();